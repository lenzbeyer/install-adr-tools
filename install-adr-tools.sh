#!/bin/bash

# Check if the installation folder is provided as a command line argument
if [ "$#" -ne 1 ]; then
  echo "Please provide the installation folder as a command line argument"
  exit 1
fi

installation_folder=$1

# Navigate to Installation Folder
cd "$installation_folder"

# Clone the Git Repository of adr-tools project
git clone https://github.com/npryce/adr-tools.git

# If you don't have a .bashrc-file, create one
touch ~/.bashrc

# Add the following lines to your .bashrc-file
echo "export PATH=\"\$PATH:$installation_folder/adr-tools/src\"" >> ~/.bashrc
echo 'export ADR_PAGER=less' >> ~/.bashrc
echo 'export LESS=-FXR' >> ~/.bashrc

# reload the .bashrc-file
source ~/.bashrc

# verify that the installation works by typing adr
if command -v adr &>/dev/null; then
  echo "adr-tools installed successfully"
else
  echo "adr-tools installation failed"
fi

